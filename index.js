// Теоретический вопрос
//
// Опишите своими словами, как Вы понимаете, что такое обработчик событий.

// Обработчик событий это функция, которая назначается для определенного события и срабатывает, как только описанное событие произошло.
//
//
//     Задание
// Создать поле для ввода цены с валидацией. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
//     Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//     В папке img лежат примеры реализации поля ввода и создающегося span.

window.onload = function createInputValidate() {

    const createInput = document.createElement('input');
    createInput.setAttribute('value', 'Price');
    document.querySelector('body').prepend(createInput);

    function removeItem(){
        const getSpan = document.querySelector('span');
        if(getSpan !== null) {
            getSpan.remove()
            createInput.value = '';
        }
    }

    createInput.onfocus = onFocus;

    function onFocus() {
        createInput.style.border = 'solid green';
        createInput.style.color = 'black';
        removeItem()
    }

    createInput.onblur = validatePrice;

    function validatePrice() {
    if (createInput.value > 0) {
        onBlur();
        } else {
        showErr()
        }
    }

    function onBlur() {
        createInput.style.border = '1px solid black';
        const createSpan = document.createElement('span');
        createSpan.innerHTML = `Текущая цена: ${createInput.value}`;
        document.querySelector('body').prepend(createSpan);
        const createBtn = document.createElement('button');
        createSpan.append(createBtn);
        createBtn.innerHTML = 'x';
        createInput.style.color = 'green';
        createBtn.onclick = removeItem;
    }

    function showErr() {
        createInput.style.border = 'solid red';
        const errSpan = document.createElement('span');
        errSpan.innerHTML = 'Please enter correct price.';
        document.querySelector('body').prepend(errSpan);
    }
};